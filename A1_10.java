class A1_10 {

    public static void longestSortedSubarray(int arr[]) {
        
        int max = 1, len = 1, maxIndex = 0;

        for (int i = 1; i < arr.length; i++) {

            if (arr[i] > arr[i - 1]) {
                len++;
            } else {

                if (max < len) {
                    max = len;

                    maxIndex = i - max;
                }
 
                len = 1;
            }
        }

        if (max < len) {
            max = len;
            maxIndex = arr.length - max;
        }

        for (int i = maxIndex; i < max + maxIndex; i++) {
            System.out.print(arr[i] + " ");
        }
    }
    
    public static void main(String[] args) {
        int arr[] = {2,3,5,9,4,6,4,6,9};
        longestSortedSubarray(arr);
    }
}

