public class A1_8 {

	public static int[] revers(int arr[]) {
		for (int i = 0; i < arr.length / 2; i++) {
			int t = arr[i];
			arr[i] = arr[arr.length - i - 1]; 
			arr[arr.length - i - 1] = t;
		}
//print reversed
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i] + " ");
		}
		return arr;

	}

	public static void main(String[] args) {
		int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		revers(arr);
	}

}

